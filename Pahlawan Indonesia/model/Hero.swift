//
//  Hero.swift
//  Pahlawan Indonesia
//
//  Created by Ihwan ID on 25/04/20.
//  Copyright © 2020 Ihwan ID. All rights reserved.
//

import UIKit
 
struct Hero {
    let photo: UIImage
    let name: String
    let description: String
}
